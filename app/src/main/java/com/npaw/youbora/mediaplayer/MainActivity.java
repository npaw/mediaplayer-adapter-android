package com.npaw.youbora.mediaplayer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.npaw.youbora.lib6.utils.youboraconfigutils.YouboraConfigManager;
import com.npaw.youbora.mediaplayer_adapter_android.R;

import java.util.LinkedHashMap;

public class MainActivity extends AppCompatActivity {

    public static final String NICE_BBB_KEY = "VoD HLS Nice";
    public static final LinkedHashMap<String, String> Resources;
    static {
        LinkedHashMap<String, String> map = new LinkedHashMap<>(7);
        map.put(NICE_BBB_KEY, "http://webb2.nice264.com/npaw/hls_samples/bbb/playlist.m3u8");
        map.put("Live HLS Aljazeera (L3)", "http://aljazeera-ara-apple-live.adaptive.level3.net/apple/aljazeera/arabic/160.m3u8");
        map.put("Live HLS VEVO (L3)", "http://vevoplaylist-live.hls.adaptive.level3.net/vevo/ch1/appleman.m3u8");
        map.put("VoD HLS BCove", "http://c.brightcove.com/services/mobile/streaming/index/master.m3u8?videoId=3747024716001&pubId=2564185535001");
        map.put("VoD mp4 Nice (Fastly)", "http://fastly.nicepeopleatwork.com.global.prod.fastly.net/video/sintel-1024-surround.mp4");
        map.put("VoD HLS Apple (ABR)", "http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8");
        map.put("Malformed url (error)", "http://false.url.com/1234.mp4");
        Resources = map;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.buttonMediaPlayer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MediaPlayerActivity.class));
            }
        });

        findViewById(R.id.buttonVideoView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), VideoViewActivity.class));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        // Set menu actions
        menu.findItem(R.id.youbora_config_show).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                YouboraConfigManager.getInstance().showConfig(getApplicationContext());
                return false;
            }
        });

        menu.findItem(R.id.youbora_config_reset).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                YouboraConfigManager.getInstance().showResetDialog(MainActivity.this, null);
                return false;
            }
        });

        return true;
    }
}
