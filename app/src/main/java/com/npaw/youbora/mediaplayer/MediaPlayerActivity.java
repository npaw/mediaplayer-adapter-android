package com.npaw.youbora.mediaplayer;

import android.content.DialogInterface;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.mediaplayer.MediaPlayerAdapter;
import com.npaw.youbora.lib6.plugin.Options;
import com.npaw.youbora.lib6.plugin.Plugin;
import com.npaw.youbora.lib6.utils.youboraconfigutils.YouboraConfigManager;
import com.npaw.youbora.mediaplayer_adapter_android.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by joan on 19/07/16.
 */
public class MediaPlayerActivity extends AppCompatActivity implements SurfaceHolder.Callback {

    private SurfaceView surfaceView;
    private MediaPlayer mediaPlayer;

    private Plugin pluginMediaPlayer;
    private MediaPlayerAdapter mediaPlayerAdapter;

    private String currentResource = MainActivity.NICE_BBB_KEY;
    private Integer seekDelta = 15000;

    // Lifecycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        Button buttonSeek = (Button) findViewById(R.id.buttonSeek);
        final Button buttonPause = (Button) findViewById(R.id.buttonPause);
        buttonSeek.setVisibility(View.VISIBLE);
        buttonPause.setVisibility(View.VISIBLE);

        // Set Youbora log level
        YouboraLog.setDebugLevel(YouboraLog.Level.VERBOSE);

        // Click listeners
        buttonPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    if (mediaPlayer.isPlaying()) {
                        buttonPause.setText("RESUME");
                        mediaPlayer.pause();
                    } else {
                        buttonPause.setText("PAUSE");
                        mediaPlayer.start();
                    }
                } catch (Exception e) {

                }
            }
        });

        buttonSeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int newPosition = mediaPlayer.getCurrentPosition() + seekDelta;
                    mediaPlayer.seekTo(Math.min(newPosition, mediaPlayer.getDuration() - 1000));
                } catch (Exception e) {

                }
            }
        });

        buttonSeek.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MediaPlayerActivity.this);
                final List<Integer> listValues = new ArrayList<>(6);
                final List<String> listDescription = new ArrayList<>(6);

                listValues.add(15000);
                listDescription.add("15 sec");
                listValues.add(30000);
                listDescription.add("30 sec");
                listValues.add(60000);
                listDescription.add("1 min");
                listValues.add(300000);
                listDescription.add("5 min");
                listValues.add(600000);
                listDescription.add("10 min");
                listValues.add(1800000);
                listDescription.add("30 min");

                builder.setTitle("Choose seek delta (current: " + listDescription.get(listValues.indexOf(seekDelta)) + ")");
                builder.setNegativeButton("Cancel", null);

                CharSequence[] cs = listDescription.toArray(new CharSequence[listDescription.size()]);

                builder.setItems(cs, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        seekDelta = listValues.get(which);
                    }
                });

                builder.show();
                return true;
            }
        });

        findViewById(R.id.buttonYouboraConfig).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YouboraConfigManager.getInstance().showConfig(getApplicationContext());
            }
        });

        findViewById(R.id.buttonResetYoubora).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YouboraConfigManager.getInstance().showResetDialog(MediaPlayerActivity.this, null);
            }
       });

        findViewById(R.id.buttonChangeAsset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MediaPlayerActivity.this);
                final List<String> listKeys = new ArrayList<String>(MainActivity.Resources.size());
                final List<String> listDescription = new ArrayList<String>(MainActivity.Resources.size());

                for (Map.Entry<String, String> entry : MainActivity.Resources.entrySet()) {
                    listDescription.add(entry.getKey() + ": " + entry.getValue());
                    listKeys.add(entry.getKey());
                }

                builder.setTitle("Change asset (current: " + currentResource + ")");
                builder.setNegativeButton("Cancel", null);

                CharSequence[] cs = listDescription.toArray(new CharSequence[listDescription.size()]);

                builder.setItems(cs, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        currentResource = listKeys.get(which);
                        Toast.makeText(getApplicationContext(), "Selected: " + currentResource, Toast.LENGTH_SHORT).show();
                    }
                });

                builder.show();
            }
        });

        findViewById(R.id.buttonPlay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPlayback();
            }
        });

        findViewById(R.id.buttonStop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlayback();
            }
        });
    }

    private void stopPlayback() {
        if(pluginMediaPlayer != null && pluginMediaPlayer.getAdapter() != null){
            ((MediaPlayerAdapter)pluginMediaPlayer.getAdapter()).fireStop();
        }
        mediaPlayer.stop();
    }

    private void startPlayback() {

        mediaPlayer.reset();

        // Load resource
        try {
            mediaPlayer.setDataSource(getApplicationContext(), Uri.parse(MainActivity.Resources.get(currentResource)));
        } catch (IOException e) {
            Log.d(getClass().getName(), "Exception while setting data source: " + e.toString());
            e.printStackTrace();
        }

        mediaPlayer.prepareAsync();

        if(mediaPlayerAdapter == null){
            pluginMediaPlayer = new Plugin(YouboraConfigManager.getInstance().getOptions(this));
            pluginMediaPlayer.setApplicationContext(getApplicationContext());
            mediaPlayerAdapter = new MediaPlayerAdapter(mediaPlayer);
            pluginMediaPlayer.setAdapter(mediaPlayerAdapter);
            ((TextView) findViewById(R.id.textViewPluginVersion)).setText(pluginMediaPlayer.getPluginVersion() + " (" + pluginMediaPlayer.getPlayerVersion() + ")");
        }
        if(pluginMediaPlayer.getAdapter().getFlags().isStarted()){
            pluginMediaPlayer.getAdapter().fireStop();
        }
        pluginMediaPlayer.getOptions().setContentResource(MainActivity.Resources.get(currentResource));

    }

    @Override
    protected void onResume() {
        super.onResume();

        // Player creation
        getWindow().setFormat(PixelFormat.UNKNOWN);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();

        mediaPlayer = new MediaPlayer();

        surfaceHolder.addCallback(this);

        // Player listeners
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mp) {
                ((MediaPlayerAdapter)pluginMediaPlayer.getAdapter()).onPrepared(mp);
                mediaPlayer.start();
                Log.d("MainActivity", "--- Client OnPreparedListener ---");
            }
        });

        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                if(what == MediaPlayer.MEDIA_ERROR_UNKNOWN || what == -38){
                    return false;
                }
                ((MediaPlayerAdapter)pluginMediaPlayer.getAdapter()).onError(mp, what, extra);
                Log.d("MainActivity", "--- Client OnErrorListener ---");
                return false;
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                ((MediaPlayerAdapter)pluginMediaPlayer.getAdapter()).onCompletion(mp);
                Log.d("MainActivity", "--- Client OnCompletionListener ---");
            }
        });

        if(pluginMediaPlayer != null && pluginMediaPlayer.getAdapter() != null){
            pluginMediaPlayer.getAdapter().setPlayer(mediaPlayer);
        }

        // Youbora plugin creation
        if(mediaPlayerAdapter == null){
            Options youboraOptions = YouboraConfigManager.getInstance().getOptions(this);
            pluginMediaPlayer = new Plugin(youboraOptions, this);
            mediaPlayerAdapter = new MediaPlayerAdapter(mediaPlayer);
            pluginMediaPlayer.setAdapter(mediaPlayerAdapter);
        }

        // Show the version
        //((TextView) findViewById(R.id.textViewPluginVersion)).setText(pluginMediaPlayer.getPluginVersion() + " (" + pluginMediaPlayer.getPlayerVersion() + ")");
    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayer.pause();
        if(pluginMediaPlayer != null && pluginMediaPlayer.getAdapter() != null){
            ((MediaPlayerAdapter)pluginMediaPlayer.getAdapter()).fireStop();
        }
    }

    // Surface listener callbacks
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mediaPlayer.setDisplay(holder);
        } catch (IllegalStateException e) {

        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) { }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) { }
}
