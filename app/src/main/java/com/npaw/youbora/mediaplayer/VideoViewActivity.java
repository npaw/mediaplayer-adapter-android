package com.npaw.youbora.mediaplayer;

import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.VideoView;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.mediaplayer.VideoViewAdapter;
import com.npaw.youbora.lib6.plugin.Options;
import com.npaw.youbora.lib6.plugin.Plugin;
import com.npaw.youbora.lib6.utils.youboraconfigutils.YouboraConfigManager;
import com.npaw.youbora.mediaplayer_adapter_android.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VideoViewActivity extends AppCompatActivity {

    private VideoView videoView;

    private Plugin youboraPlugin;
    private VideoViewAdapter adapter;

    private String currentResource = MainActivity.NICE_BBB_KEY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        videoView = (VideoView) findViewById(R.id.videoView);

        // Set log level
        YouboraLog.setDebugLevel(YouboraLog.Level.VERBOSE);

        // Click listeners
        findViewById(R.id.buttonYouboraConfig).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YouboraConfigManager.getInstance().showConfig(getApplicationContext());
            }
        });

        findViewById(R.id.buttonResetYoubora).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YouboraConfigManager.getInstance().showResetDialog(VideoViewActivity.this,null);
            }
        });

        findViewById(R.id.buttonChangeAsset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(VideoViewActivity.this);
                final List<String> listKeys = new ArrayList<String>(MainActivity.Resources.size());
                final List<String> listDescription = new ArrayList<String>(MainActivity.Resources.size());

                for (Map.Entry<String, String> entry : MainActivity.Resources.entrySet()) {
                    listDescription.add(entry.getKey() + ": " + entry.getValue());
                    listKeys.add(entry.getKey());
                }

                builder.setTitle("Change asset (current: " + currentResource + ")");
                builder.setNegativeButton("Cancel", null);

                CharSequence[] cs = listDescription.toArray(new CharSequence[listDescription.size()]);

                builder.setItems(cs, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        currentResource = listKeys.get(which);
                        Toast.makeText(getApplicationContext(), "Selected: " + currentResource, Toast.LENGTH_SHORT).show();
                    }
                });

                builder.show();
            }
        });

        findViewById(R.id.buttonPlay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPlayback();
            }
        });

        findViewById(R.id.buttonStop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlayback();
            }
        });

        MediaController mediaController = new MediaController(VideoViewActivity.this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mp) {
                ((VideoViewAdapter)youboraPlugin.getAdapter()).onPrepared(mp);
                Log.d("MainActivity", "--- Client OnPreparedListener ---");
            }
        });

        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                ((VideoViewAdapter)youboraPlugin.getAdapter()).onError(mp, what, extra);
                Log.d("MainActivity", "--- Client OnErrorListener ---");
                return false;
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                ((VideoViewAdapter)youboraPlugin.getAdapter()).onCompletion(mp);
                Log.d("MainActivity", "--- Client OnCompletionListener ---");
            }
        });

        // Youbora plugin creation
        Options youboraOptions = YouboraConfigManager.getInstance().getOptions(this);
        youboraPlugin = new Plugin(youboraOptions, this);
        adapter = new VideoViewAdapter(videoView);

        // Show the version
        //((TextView) findViewById(R.id.textViewPluginVersion)).setText(pluginVideoView.getPluginVersion() + " (" + pluginVideoView.getPlayerVersion() + ")");
    }

    private void stopPlayback(){
        (youboraPlugin.getAdapter()).fireStop();
        videoView.stopPlayback();
    }

    private void startPlayback() {

        // Load resource
        Uri video = Uri.parse(MainActivity.Resources.get(currentResource));
        videoView.setVideoURI(video);

        youboraPlugin.setOptions(YouboraConfigManager.getInstance().getOptions(VideoViewActivity.this));

        // Inform Youbora the resource
        youboraPlugin.getOptions().setContentResource( MainActivity.Resources.get(currentResource));
        if(youboraPlugin.getAdapter() == null){
            youboraPlugin.setAdapter(adapter);
        }else{
            youboraPlugin.getAdapter().fireStop();
        }


        // Start playback
        videoView.start();

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        videoView.resume();
        super.onResume();
        youboraPlugin.setOptions(YouboraConfigManager.getInstance().getOptions(getApplicationContext()));
    }

    @Override
    protected void onPause() {
        videoView.pause();
        super.onPause();
        videoView.stopPlayback();
        if(youboraPlugin != null && youboraPlugin.getAdapter() != null){
            youboraPlugin.getAdapter().fireStop();
        }
    }
}
