package com.npaw.youbora.lib6.mediaplayer;

import android.widget.VideoView;

/**
 * Created by Enrique on 07/03/2018.
 */
public class VideoViewAdapter extends AbstractAdapterMediaPlayer<VideoView> {

    public VideoViewAdapter(VideoView player){
        super(player);

        if (getMonitor() != null) getMonitor().skipNextTick();
    }

    @Override
    boolean isPlaying() {
        boolean playing;

        try {
            playing = getPlayer().isPlaying();
        } catch (Exception e) {
            playing = false;
        }

        return playing;
    }

    @Override
    protected int getCurrentPosition() {
        int position;

        try {
            position = getPlayer().getCurrentPosition();
        } catch (Exception e) {
            position = 0;
        }

        return position;
    }

    @Override
    protected int getMediaDuration() {
        int duration;

        try {
            duration = getPlayer().getDuration();
        } catch (Exception e) {
            duration = 0;
        }

        return duration;
    }

    @Override
    public String getPlayerName() { return "VideoView"; }
}
