package com.npaw.youbora.lib6.mediaplayer;

import android.media.MediaPlayer;

/**
 * Created by Enrique on 07/03/2018.
 */
public class MediaPlayerAdapter extends AbstractAdapterMediaPlayer<MediaPlayer> {

    public MediaPlayerAdapter(MediaPlayer player){
        super(player);
    }

    @Override
    boolean isPlaying() {
        boolean playing;

        try {
            playing = getPlayer().isPlaying();
        } catch (Exception e) {
            playing = false;
        }

        return playing;
    }

    @Override
    int getCurrentPosition() {
        int position;

        try {
            position = getPlayer().getCurrentPosition();
        } catch (Exception e) {
            position = 0;
        }

        return position;
    }

    @Override
    int getMediaDuration() {
        int duration;

        try {
            duration = getPlayer().getDuration();
        } catch (Exception e) {
            duration = 0;
        }

        return duration;
    }
}
