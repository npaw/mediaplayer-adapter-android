package com.npaw.youbora.lib6.mediaplayer;

import android.media.MediaPlayer;

import com.npaw.youbora.lib6.Timer;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.adapter.PlayerAdapter;
import com.npaw.youbora.lib6.adapter.PlayheadMonitor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Enrique on 07/03/2018.
 */
public abstract class AbstractAdapterMediaPlayer<PlayerT> extends PlayerAdapter<PlayerT>
        implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener,
        MediaPlayer.OnErrorListener {

    private Timer startJoinDetectorTimer = null;
    private boolean prepared;

    private Integer lastErrorReported = null;
    private Integer lastErrorMsgReported = null;

    // Subclasses must implement these methods
    abstract boolean isPlaying();
    abstract int getCurrentPosition();
    abstract int getMediaDuration();

    /**
     * Constructor.
     * When overriding it, make sure to call super.
     * Implement the logic to register to the player events in the {@link #registerListeners()} method.
     * Usually you will want to call {@link #registerListeners()} from the overridden constructor.
     *
     * @param player the player instance this Adapter will be bounded to.
     */
    public AbstractAdapterMediaPlayer(PlayerT player) {
        super(player);

        registerListeners();

        //this.player = player;

        prepared = false;
    }



    @Override
    public void registerListeners() {

        /*
         * As there is no way of knowing when the player is paused/resumed, the checkPlayhead
         * is used in order to detect them. Update the playing/pause status just before
         * checking for buffers and seeks to avoid false positive buffer detection
         */
        setMonitor(new PlayheadMonitor(this,
                PlayheadMonitor.TYPE_BUFFER | PlayheadMonitor.TYPE_SEEK,800) {
            @Override
            public void progress() {

                boolean playing = isPlaying();

                if (prepared && getFlags().isJoined()) {
                    if (playing) {
                        if (getFlags().isPaused()) fireResume();
                    } else {
                        if (!getFlags().isPaused()) firePause();
                    }
                }

                super.progress();
            }
        });

        listenForJoinTime();

        super.registerListeners();
    }

    protected static String errorCodeToString(int errorCode) {
        String error;

        switch (errorCode) {
            case MediaPlayer.MEDIA_ERROR_IO:
                error = "Media Error IO";
                break;
            case MediaPlayer.MEDIA_ERROR_MALFORMED:
                error = "Media Error Malformed";
                break;
            case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                error = "Media Error Not valid for progressive playback";
                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                error = "Media Error server died";
                break;
            case MediaPlayer.MEDIA_ERROR_TIMED_OUT:
                error = "Media Error timed out";
                break;
            case MediaPlayer.MEDIA_ERROR_UNSUPPORTED:
                error = "Media Error unsupported";
                break;
            case MediaPlayer.MEDIA_ERROR_UNKNOWN:
            default:
                //Special case for in-stream errors
                error = "Media Error Unknown";
                break;
        }

        return error;
    }

    @Override
    public void unregisterListeners() {
        super.unregisterListeners();

        if (startJoinDetectorTimer != null) startJoinDetectorTimer.stop();
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        YouboraLog.debug("onPrepared");
        if (getPlayer() == null) {
            YouboraLog.debug("onPrepared called but no session is active, ignoring event");
        } else {
            prepared = true;

            try {
                if (getFlags().isStarted()) fireStop();

                fireStart();

                if (!getFlags().isJoined()) listenForJoinTime();
            } catch (Exception e){
                YouboraLog.error(e);
            }
        }
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        YouboraLog.debug("onCompetion");

        if (getPlayer() == null) {
            YouboraLog.debug("onCompletion called but no session is active, ignoring event");
        } else {
            fireStop();
        }
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
        YouboraLog.debug("onError");

        if (getPlayer() == null) {
            YouboraLog.debug("onError called but no session is active, ignoring event");
        } else {

            //Too many error unknowns, so we don't report it and let customer handle it
            /*if(what == MediaPlayer.MEDIA_ERROR_UNKNOWN || what == -38){
                return false;
            }*/

            if (lastErrorReported == null || (lastErrorMsgReported != null &&
                    lastErrorReported != extra && lastErrorMsgReported != what)) {
                String errorMessage = errorCodeToString(what);
                //I know it's the -38 error but I can't find out the constant know the constant
                /*if(extra == -38){
                    errorMessage = "In-Stream error";
                }*/
                if (!getFlags().isStarted() &&
                        what == MediaPlayer.MEDIA_ERROR_UNKNOWN && extra == 0) {
                    return false;
                }

                fireError(errorMessage, Integer.toString(extra), "");

                if (what == MediaPlayer.MEDIA_ERROR_TIMED_OUT) fireStop();

                lastErrorReported = extra;
                lastErrorMsgReported = what;
            }
        }

        if (startJoinDetectorTimer != null) startJoinDetectorTimer.stop();

        return false;
    }

    private void listenForJoinTime() {
        startJoinDetectorTimer = new Timer(new Timer.TimerEventListener(){
            @Override
            public void onTimerEvent(long delta) {
                boolean playing = isPlaying();

                if (getPlayhead() != null) {
                    if (getPlayhead() > 0 && playing) fireStart();

                    if (getPlayhead() > 0.1 && getFlags().isStarted() && !getFlags().isJoined()) {
                        fireJoin();
                        if (getMonitor() != null) getMonitor().skipNextTick();
                    }
                }
            }
        },100);

        startJoinDetectorTimer.start();
    }

    @Override
    public Double getPlayhead() {
        return getCurrentPosition() / 1000.0;
    }

    @Override
    public Double getDuration() {
        Double duration = prepared ? getMediaDuration() / 1000.0 : 0;

        if (duration <= 0) duration = super.getDuration();

        return duration;
    }

    @Override
    public String getVersion() {
        return BuildConfig.VERSION_NAME + "-" + getPlayerName();
    }

    @Override
    public String getPlayerName() {
        return "MediaPlayer";
    }

    @Override
    public void fireStop(Map<String, String> params) {
        super.fireStop(new HashMap<String, String>(1){{
            put("playhead","-1");
        }});

        prepared = false;

        if (startJoinDetectorTimer != null) startJoinDetectorTimer.stop();
    }
}
