## [6.7.1] - 2021-04-14
### Modified
- Deployment platform moved from Bintray to JFrog.

## [6.7.0] - 2020-03-17
### Refactored
- Adapter to work with the new Youboralib version.

## [6.4.0] - 2019-07-10
### Added
- Youboralib updated to version 6.4.7.

## [6.0.0] - 2018-06-18
### Added
- First release.